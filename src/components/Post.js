import { Favorite, FavoriteBorder, MoreVert, Share } from "@mui/icons-material";
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Checkbox,
  IconButton,
  Skeleton,
  Typography,
} from "@mui/material";
import React, { useState } from "react";

function Post() {
  const [loading, setLoading] = useState(true);

  setTimeout(() => {
    setLoading(false);
  }, [3000]);

  return (
    <Card sx={{ marginBottom: 3 }}>
      <CardHeader
        avatar={
          loading ? (
            <Skeleton
              variant="circular"
              animation="wave"
              width={40}
              height={40}
            />
          ) : (
            <Avatar sx={{ bgcolor: "red" }} aria-label="recipe">
              P
            </Avatar>
          )
        }
        action={
          loading ? null : (
            <IconButton aria-label="settings">
              <MoreVert />
            </IconButton>
          )
        }
        title={
          loading ? (
            <Skeleton
              animation="wave"
              width="40%"
              height={20}
              //   style={{ marginBottom: 6 }}
            />
          ) : (
            "Pankaj Sharma"
          )
        }
        subheader={
          loading ? (
            <Skeleton
              animation="wave"
              width="50%"
              height={20}
              //   style={{ marginBottom: 6 }}
            />
          ) : (
            "September 3, 2022"
          )
        }
      />
      {loading ? (
        <Skeleton animation="wave" height={300} variant="rectangular" />
      ) : (
        <CardMedia
          component="img"
          height="300px"
          image="https://images.pexels.com/photos/4534200/pexels-photo-4534200.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
          alt="Paella dish"
        />
      )}

      <CardContent>
        {loading ? (
          <>
            <Skeleton
              animation="wave"
              height={20}
              style={{ marginBottom: 6 }}
            />
            <Skeleton animation="wave" height={20} width="70%" />
          </>
        ) : (
          <Typography variant="body2" color="text.secondary">
            This impressive paella is a perfect party dish and a fun meal to
            cook together with your guests. Add 1 cup of frozen peas along with
            the mussels, if you like.
          </Typography>
        )}
      </CardContent>
      <CardActions disableSpacing>
        {loading ? (
          <Skeleton
            variant="circular"
            animation="wave"
            width={40}
            height={40}
            sx={{ marginRight: 1 }}
          />
        ) : (
          <Checkbox
            icon={<FavoriteBorder />}
            checkedIcon={<Favorite sx={{ color: "red" }} />}
          />
        )}
        {loading ? (
          <Skeleton
            variant="circular"
            animation="wave"
            width={40}
            height={40}
          />
        ) : (
          <IconButton aria-label="share">
            <Share />
          </IconButton>
        )}
      </CardActions>
    </Card>
  );
}

export default Post;
